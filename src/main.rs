use clap::Parser;
use scraper::{Html, Selector};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Scrape Search Results from DuckDuckgo.com
#[derive(Parser, Debug)]
#[clap(version, about, long_about = None)]
struct Args {
    /// Keyword to search for
    query: String,
    #[clap(short, long, value_parser, default_value_t = 10)]
    /// how many approx results you would like to see
    count: u8,
}

#[derive(Serialize, Deserialize)]
struct SearchResult {
    link: String,
    desc: String,
}

#[derive(Debug)]
pub enum DuckError {
    NoResults,
    JsonError(serde_json::Error),
    Reqewst(reqwest::Error),
    HtmlChanged(String),
}
impl From<serde_json::Error> for DuckError {
    fn from(se: serde_json::Error) -> DuckError {
        DuckError::JsonError(se)
    }
}
impl From<reqwest::Error> for DuckError {
    fn from(re: reqwest::Error) -> DuckError {
        DuckError::Reqewst(re)
    }
}

pub fn scrape(query: String, res_count: u8) -> Result<String, DuckError> {
    let req_client = reqwest::blocking::Client::builder()
        .user_agent("Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")
        .build()?;

    let mut req_params = HashMap::new();
    req_params.insert("q".to_owned(), query.clone()); // query text
    req_params.insert("kp".to_owned(), "-2".into()); // safe search off
    req_params.insert("kv".to_owned(), "-1".into()); // no ads
    req_params.insert("kl".to_owned(), "wt-wt".into()); // no region

    let mut results: Vec<SearchResult> = vec![];

    while results.len() <= res_count.into() {
        let response = req_client
            .post("https://html.duckduckgo.com/html")
            .form(&req_params)
            .send()?;

        let document = Html::parse_document(&response.text()?);
        // pushoing the result links of a page to a Vec
        let article_selector = Selector::parse("a.result__a").expect("html page to stay same");
        document
            .select(&article_selector)
            .map(|x| SearchResult {
                link: x.value().attr("href").unwrap_or("NO_LINK").to_string(),
                desc: x.inner_html(),
            })
            .for_each(|sr| results.push(sr));

        let nav_link = Selector::parse("div.nav-link").expect("html page to stay same");
        let inputs = Selector::parse("input").expect("html page to stay same");

        // get and set params for next page results
        let next_form = document
            .select(&nav_link)
            .map(|f| f.inner_html())
            .filter(|f| f.contains("Next"))
            .map(|f| Html::parse_fragment(&f))
            .next()
            .ok_or(DuckError::NoResults)?;

        next_form
            .select(&inputs)
            .map(|x| x.value())
            .filter(|x| x.attr("value").unwrap_or("") != "Next")
            .for_each(|input| {
                let name = input.attr("name").unwrap_or("");
                let value = input.attr("value").unwrap_or("");
                req_params.insert(name.to_owned(), value.to_owned());
            });
    }
    Ok(serde_json::to_string(&results)?)
}

fn main() {
    let args = Args::parse();

    std::process::exit(match scrape(args.query, args.count) {
        Ok(json_out) => {
            println!("{}", json_out);
            0
        }
        Err(DuckError::NoResults) => {
            eprintln!("No Results were found, try a diffrent query");
            1
        }
        Err(e) => {
            eprintln!("{:?}", e);
            1
        }
    });
}
